import 'package:json_annotation/json_annotation.dart';
part 'notifications.g.dart';
@JsonSerializable()
class Notification {
  String id;
  String type;
  String title;
  @JsonKey(name: 'user_id')
  int userId;
  @JsonKey(name: 'user_avatar')
  String userAvatar;
  String message;
  Notification({
    this.type,this.id,this.message,this.title,this.userId,this.userAvatar
});
  factory Notification.fromJson(Map<String,dynamic> json) => _$NotificationFromJson(json);
  Map<String, dynamic> toJSON() => _$NotificationToJson(this);

}