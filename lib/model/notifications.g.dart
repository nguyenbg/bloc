// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notification _$NotificationFromJson(Map<String, dynamic> json) {
  return Notification(
    type: json['type'] as String,
    id: json['id'] as String,
    message: json['message'] as String,
    title: json['title'] as String,
    userId: json['user_id'] as int,
    userAvatar: json['user_avatar'] as String,
  );
}

Map<String, dynamic> _$NotificationToJson(Notification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'title': instance.title,
      'user_id': instance.userId,
      'user_avatar': instance.userAvatar,
      'message': instance.message,
    };
