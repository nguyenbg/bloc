import 'package:json_annotation/json_annotation.dart';
import 'package:lib_bloc/model/notifications.dart';
import 'api_response.dart';
part 'notification_response.g.dart';
@JsonSerializable()
class NotificationResponse extends ApiResponse{
 List<Notification> data;
  NotificationResponse({this.data});
  factory NotificationResponse.fromJson(Map<String,dynamic> json) =>
      _$NotificationResponseFromJson(json);
 Map<String, dynamic> toJSON() => _$NotificationResponseToJson(this);
}