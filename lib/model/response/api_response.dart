abstract class ApiResponse extends Object {
  bool success;
  String errorCode;
  String message;
  String nextUrl;
}