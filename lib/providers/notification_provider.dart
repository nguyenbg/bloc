import 'package:lib_bloc/contains/base_url.dart';
import 'package:lib_bloc/providers/base_provider.dart';
import 'package:lib_bloc/model/response/notification_response.dart';
class NotificationProvider extends BaseProvider{
  Future<NotificationResponse> ListNotification ({String uri }) async{
   if(uri==null){
     uri =BaseUrl.NOTIFICATION;
   }
    var jsonData = await this.get(uri);
    return NotificationResponse.fromJson(jsonData);
  }
}