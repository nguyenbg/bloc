import 'Count_event.dart';
import 'Count_state.dart';
import 'package:bloc/bloc.dart';
class CountBloc extends Bloc<CountEvent,CounterState>{
  int count =0;
  @override
  // TODO: implement initialState
  CounterState get initialState => Inital();

  @override
  Stream<CounterState> mapEventToState(CountEvent event) async*{
  if(event is CounterIncreasing){
    this.count++;
    yield(CounterIncrease());
  }
  if(event is CounterReducting){
    this.count--;
    yield(CounterReduction());
  }
  }

}