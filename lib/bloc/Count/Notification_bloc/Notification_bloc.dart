import 'dart:core' as prefix0;
import 'dart:core';

import 'package:lib_bloc/bloc/Count/Notification_bloc/Notification_event.dart';
import 'package:lib_bloc/bloc/Count/Notification_bloc/Notification_state.dart';
import 'package:bloc/bloc.dart';
import 'package:lib_bloc/model/notifications.dart';
import 'package:lib_bloc/providers/notification_provider.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  List<Notification> listdata = [];
  String nextUrl = '';


  NotificationProvider _notificationProvider = new NotificationProvider();

  @override
  get initialState => InitalNotification();

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is LoadNotification) {
      if (currentState is InitalNotification) {


        var response = await _notificationProvider.ListNotification();
        if (response.success) {
          this.listdata = response.data;
          nextUrl = response.nextUrl;
          yield NotificationLoaded();
        }
      }
    }
    if(event is LoadMoreNotification){
      var response =
      await _notificationProvider.ListNotification(uri: nextUrl);
      listdata += response.data;
      nextUrl = response.nextUrl;
      yield NotificationLoaded();
    }
  }
}
