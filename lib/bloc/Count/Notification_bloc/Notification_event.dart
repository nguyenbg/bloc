abstract class NotificationEvent {
  NotificationEvent([List props = const []]) : super();
}

class LoadNotification extends NotificationEvent {}

class LoadMoreNotification extends NotificationEvent {}
