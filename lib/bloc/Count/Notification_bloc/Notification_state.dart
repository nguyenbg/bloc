abstract class NotificationState{
  NotificationState([List props = const []]) : super();
}
class InitalNotification extends NotificationState{}
class NotificationLoading extends NotificationState{}
class NotificationLoaded extends NotificationState{}
