import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:lib_bloc/bloc/Count/Notification_bloc/Notification_state.dart';
import 'package:lib_bloc/bloc/Count/Notification_bloc/Notification_event.dart';
import 'package:lib_bloc/bloc/Count/Notification_bloc/Notification_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  final String uri = 'http://192.168.1.37:3000/nguyen/notification';

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int lenght;
  NotificationBloc _notificationBloc = new NotificationBloc();
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    _notificationBloc.dispatch(LoadNotification());

    scrollController.addListener(() {
      final maxScroll = scrollController.position.maxScrollExtent;
      final currentScroll = scrollController.position.pixels;

      if (maxScroll == currentScroll && _notificationBloc.nextUrl != null) {
        _notificationBloc.dispatch(LoadMoreNotification());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
        bloc: _notificationBloc,
        condition: (prevState, currentState) {
          if (currentState is NotificationLoading) return false;
          return true;
        },
        builder: (BuildContext context, NotificationState state) {
          if (state is InitalNotification) {
            print("inital");
            return Center(child: CupertinoActivityIndicator());
          }
          if (state is NotificationLoaded) {
            lenght = _notificationBloc.listdata.length;
            if (_notificationBloc.nextUrl != null) {
              lenght++;
            }
            print(lenght);
          }

          return CupertinoPageScaffold(
            child: ListView.builder(
                controller: scrollController,
                itemCount: lenght,
                itemBuilder: (BuildContext context, int index) {
                  if (index == lenght - 1) {
                    return CupertinoActivityIndicator();
                  }
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            border:
                                Border(bottom: BorderSide(color: Colors.grey))),
                        height: 72.0,
                        child: Row(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 32.0,
                              backgroundImage: NetworkImage(_notificationBloc
                                      .listdata[index].userAvatar ??
                                  'http://thuthuatphanmem.vn/uploads/2018/09/11/hinh-anh-dep-6_044127357.jpg'),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(index.toString()),
                                  Text(_notificationBloc
                                          .listdata[index].message ??
                                      'null'),
                                  Text(
                                      _notificationBloc.listdata[index].title ??
                                          'null')
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  );
                }),
          );
        },
      ),
    );
  }
}
